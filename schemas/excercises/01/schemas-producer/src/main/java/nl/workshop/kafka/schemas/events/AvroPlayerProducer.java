package nl.workshop.kafka.schemas.events;


import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import nl.workshop.kafka.avro.schemas.Player;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Value;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Service;

import java.util.Properties;

@Service
public class AvroPlayerProducer {

    @Value("${spring.kafka.producer.bootstrap-servers}")
    private String bootstrapServers;

    public void sendMessage(Player player) {
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");

        Producer<Object,Object> producer = new KafkaProducer<>(properties);

        ProducerRecord<Object,Object> record = new ProducerRecord<>("avro.topic", null, player);

        producer.send(record);

        producer.flush();
        producer.close();
    }
}
