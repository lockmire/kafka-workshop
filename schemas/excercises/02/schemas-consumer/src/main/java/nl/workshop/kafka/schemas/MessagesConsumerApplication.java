package nl.workshop.kafka.schemas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MessagesConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MessagesConsumerApplication.class, args);
	}

}
