package nl.workshop.kafka.schemas.events;


import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import nl.workshop.kafka.avro.schemas.Message;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Value;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Properties;

@Service
public class AvroMessageProducer {

    private final String topic;
    private final KafkaTemplate<String, Message> template;

    public AvroMessageProducer(@Value("${kafka.topic.message}") String topic, KafkaTemplate<String, Message> template) {
        this.topic = topic;
        this.template = template;
    }

    public void sendMessage(Message message) {
        ProducerRecord<String,Message> record = new ProducerRecord<>(topic, null, message);
        template.send(record);
    }
}
