# Exercise: Kafka streams
## Compacted topic
1. Create a folder named "model" in the consumer project.
2. Create the following classes including getters and setters in the model folder. 
    ```
        Question {
            private String id;
            private String question;
            private List<String> answers;
            private String correctAnswer;
        }
        
        PlayerAnswer {
            private String playerId;
            private String questionId;
            private String answer;
        }
    ```  
3. Change the player topic in the KafkaSetUpConfig of the consumer so it's a compacted topic. More information on how to make a topic compacted can be found here:  https://medium.com/@TimvanBaarsen/programmatically-create-kafka-topics-using-spring-kafka-8db5925ed2b1.
4. Now create two new topics in the KafkaSetUpConfig with the same configuration as the player topic. One named "question" and the other named "player-answer".
These values come from the properties "kafka.topic.question" and kafka.topic.player-answer" in the application.properties file. Make the "question" topic compacted.
5. If you have your Kafka and zookeeper instance running stop both and delete all the topics. That can be done by the following commands in your Kafka folder:
    ```
    ./bin/zookeeper-server-stop.sh
    ./bin/kafka-server-stop.sh
    rm -rf /tmp/kafka-logs /tmp/kafka-streams /tmp/zookeeper
    ``` 
6. Now start Zookeeper and Kafka again.      
7. Now start the consumer service and check with the CLI commands if the topics have been created.

## Stateless streams
Look at the first code example in the following link. Only look at the first code snippet and don't read further than "Step 1: Download the code". https://kafka.apache.org/26/documentation/streams/quickstart

1. In the POM of the consumer add the following dependency:
    ```
    <dependency>
        <groupId>org.apache.kafka</groupId>
        <artifactId>kafka-streams</artifactId>
    </dependency>
   ``` 
2. Add the "@EnableKafkaStreams" annotation to the AdvancedConsumerApplication class.
3. Add "spring.kafka.streams.application-id=player-consumer-application" to the application properties
4. Create a class named "GameStreamBuilder" with @Component annotation in the service folder of the consumer.
5. Create a @Bean method that returns a KStream<String, String> called constructGame() with arguments: "StreamsBuilder streamsBuilder" and "@Value("${kafka.topic.player-answer}") String playerAnswerTopic".
The StreamsBuilder is a convenient topology builder provided by the autoconfiguration of Spring when you add the "@EnableKafkaStreams" annotation.
6. We will now use the StreamsBuilder to create a topology. First we need to define the Serdes that are used by KafkaStreams to serialize and deserialize the records.
The source of the topology we will build contains records with a string as the key and a PlayerAnswer object as the value. Therefore, we need to add the following Serdes:
    ```
   Serde<String> stringSerde = Serdes.String();
   JsonSerde<PlayerAnswer> playerAnswerSerde = new JsonSerde<>(PlayerAnswer.class);
   ```
7. Now write a KStream<String, String> object called "playerAnswerStream" and use the streamsbuilder to create a stream. 
The StreamsBuilder.stream() method does this for you. The method takes two arguments. The first is the playerAnswerTopic argument to specify the topic name.
The second one is optional but specifies how to consume the records. This is done with a "Consumed" object that takes two serdes. One for the key and one for the value.
Add the following as the second argument: "Consumed.with(stringSerde, playerAnswerSerde)". You know have a KStream<String, PlayerAnswer> stream!
8. Now we are going to perform some stateless stream operations on the stream. The first is a peek operation. Write ".peek()" after the ".stream()" method.
The peek method allows you to do something with the data without changing it. One example is logging the data. That is what we are going to do.
The method takes a function as an argument. The arguments of the function are key and value. So the lambda will look something like this:
    ```
   .streamsBuilder
   .stream(..)
   .peek((key, value) -> ..)
   ```
   
    When you have written the lambda extract it to a function. For instance:

    ```
    private void print(String key, PlayerAnswer playerAnswer) {
    System.out.println("key: " + key + "\n value: " + playerAnswer.toString());
    }
    ```
    This allows you to put a breakpoint in the function and helps you with debugging the stream and seeing if the stream is working as it is supposed to.
9. Now add ".filter((key, value) -> ..)" to filter out all players that do not have playerId "player1". 
10. Now add .mapValues() to only take the answer from the PlayerAnswer object.
11. Add a last ".peek((key, value) -> ..)" method to print the value. Add the return statement of the constructGame() method to return the playerAnswerStream.
12. Start the consumer application in IntelliJ.
13. Now we want to test the topology! Because we will not be creating a producer for the player answers we will use the Kafka CLI console producer. Enter the following command in the bin folder of Kafka:
    ```
    ./kafka-console-producer.sh --broker-list localhost:9092 --topic player-answer --property parse.key=true --property key.separator=:
    ```
    This will start a producer where you can also specify the key. The key and value are separated by ":". Add a breakpoint in your print method and test the topology with the following message:
    ```
    question1:{"playerId": "player1", "questionId": "question1", "answer": "Kafka"}
    ```

## Statefull streams
1. Now we are going to make our first KTable. Before we start we need to add an argument to the constructGame method. This argument is "@Value("${kafka.topic.question}") String questionTopic".
2. Then below the first two serdes add a serde for the Question class.
3. Now construct the KTable. You can construct a KTable like this: 
    ```
   GlobalKTable<String, Question> questionTable = streamsBuilder.globalTable(.., ..)
    ```
   The first argument is the questionTopic and the second is a Consumed.with() with the key and value serde needed for the question topic.
4. That's it! You have constructed a KTable. Now remove the filter, mapvalues and the last peek from the topology. So you are left with:
    ```
   KStream<String, String> playerAnswerStream = streamsBuilder
   				.stream(playerAnswerTopic, Consumed.with(stringSerde, playerAnswerSerde))
   				.peek(this::print)
   ```
    This will complain because it's assigning an object that does not have the correct arguments but we will fix that in a minute.
5. Now create an "answer-corrected" topic in KafkaSetUpConfig. It should have the same settings as the other topics and take its name from the application.properties.
6. Add "@Value("${kafka.topic.answer-corrected}") String answerCorrectedTopic" to the arguments of the "constructGame()" method
7. Add a JsonSerde for the AnswerCorrected class. 
8. Change the "KStream<String, String> playerAnswerStream" to "KStream<String, AnswerCorrected> playerAnswerStream". Also change this in the method return type.
9. Uncomment the "checkAnswer()" method in the GameStreamBuilder. 
10. Now after peek add ".join()". This will match any records in the stream and the KTable that have the same key. In the join first specify the 
table you want to join with (questionTable), then the key selector, which should use the key of the playerAnswer and then the value joiner that takes
the matched playerAnswer and question as argument and produces a new record as the result. The resulting object will be an "AnswerCorrected" object. The final join should look like this:
       ```
       .join(questionTable, (key, value) -> key, this::checkAnswer)
       ```

11. Between the stream and the return statement add:
       ```
       playerAnswerStream.to(answerCorrectedTopic, Produced.with(stringSerde, answerCorrectedSerde));
       ``` 
    
   This forwards the result of the stream to the answerCorrectedTopic.
12. Your topology is all set up now. Time to test! Use the following producer: 
    ```
    ./kafka-console-producer.sh --broker-list localhost:9092 --topic question --property parse.key=true --property key.separator=:
    ```
    This will start a producer where you can also specify the key. The key and value are separated by ":". Add a breakpoint in the checkAnswer method and see what happens when 
    you sent a question to the question topic like this and after that an answer in the other producer:
    ```
    question1:{"id": "question1", "question": "What is the best streaming platform?", "answers": ["Kafka", "Flink"], "correctAnswer": "Kafka"}
    ```

13. Now consume the answer-corrected topic and see the results:
    ```
    ./kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic answer-corrected --from-beginning
    ```
