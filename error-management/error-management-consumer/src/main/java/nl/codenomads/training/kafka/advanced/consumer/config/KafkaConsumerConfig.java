package nl.codenomads.training.kafka.advanced.consumer.config;

import nl.codenomads.training.kafka.advancedproducer.model.Player;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

@Configuration
public class KafkaConsumerConfig {

	private KafkaProperties kafkaProperties;

	public KafkaConsumerConfig(KafkaProperties kafkaProperties) {
		this.kafkaProperties = kafkaProperties;
	}

	public Map<String, Object> consumerConfig() {
		Map<String, Object> props = new HashMap<>();

		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.getBootstrapServers());
		props.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaProperties.getConsumer().getGroupId());
		props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, kafkaProperties.getConsumer().getAutoOffsetReset());
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
		props.put(JsonDeserializer.TRUSTED_PACKAGES, "*");

		return props;
	}

	@Bean
	public ConsumerFactory<String, Player> consumerFactory() {
		return new DefaultKafkaConsumerFactory<>(consumerConfig());
	}

	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, Player> kafkaListenerContainerFactory(ConsumerFactory<String, Player> consumerFactory
			// TODO: add error handler bean
	) {
		ConcurrentKafkaListenerContainerFactory<String, Player> factory = new ConcurrentKafkaListenerContainerFactory<>();
		// TODO: add error handler to factory
		factory.setConsumerFactory(consumerFactory);
		return factory;
	}

	// TODO: create SeekToCurrentErrorHandler bean

	private BiFunction<ConsumerRecord<?, ?>, Exception, TopicPartition> destinationResolver() {
		return (consumerRecord, exception) ->
				new TopicPartition(
							// TODO: instead of empty String replace the topic with <originalTopicName (from record)> + ".DLT." + the consumer group from the kafkaProperties
						""
						, consumerRecord.partition());
	}
}
