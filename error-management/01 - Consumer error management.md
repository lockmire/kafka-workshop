# Exercise 6: Consumer error management

First read the Error recovery part of: https://www.confluent.io/blog/spring-for-apache-kafka-deep-dive-part-1-error-handling-message-conversion-transaction-support/ (read until Deserialization errors)
We will be implementing a **slightly changed version** of the DLT logic that is described here.

1. Because we will be publishing to a DLT we need a Kafka template to forward the messages. The beans for this template need to be defined in the KafkaProducerConfig.
Go to the KafkaProducerConfig and add the beans.
2. Then add the creation of the DLT to the KafkaSetupConfig. Do this in the TODO part of the NewTopic bean.
Add the name based on <topicname> + ".DLT." + <consumer group> based on the properties in application.properties. 
The topic has the same number of partitions and the same replication factor as the player topic.

3. In the KafkaConsumerConfig add a bean for a SeekToCurrentErrorHandler. This Bean takes "KafkaOperations<String, Object> template" as argument.
KafkaOperations is an interface of which KafkaTemplate is an implementation. The KafkaTemplate bean created in the KafkaProducerConfig will be injected here.
4. In the SeekToCurrentErrorHandler bean method create a DeadLetterPublishingRecoverer object. The constructor of this object takes the template
and a destinationResolving method. Use the template and destinationResolver() as arguments.
5. Implement the destinationResolver() destination topic logic at the TODO comment.

6. Use https://docs.spring.io/spring-kafka/reference/html/#seek-to-current as inspiration. 
Now in the SeekToCurrentErrorHandler bean return a SeekToCurrentErrorHandler that takes the DeadLetterPublishingRecoverer object and a FixedBackOff object as arguments.
The FixedBackOff object has an interval of 1000 millis and maxAttempts 2.
7. Set the errorHandler in the ConcurrentKafkaListenerContainerFactory bean:
    1. Add the SeekToCurrentErrorHandler as input argument to the kafkaListenerContainerFactory() method.
    2. Set the error handler on the factory
8. Now the error handling logic is implemented. Test it by throwing a runtime exception in the listener method. 
9. Test with the debugger that the message is received 3 times by the method with the @KafkaListener annotation.
10. After that the message will be written to the topic player.DLT.player-consumer-group. Check this with the Kafka CLI tools.