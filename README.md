# Kafka workshop

This repository contains the supporting materials for a hands-on Kafka workshop. 
The repository consists of four main sections, which each have their own maven module with their potential submodules.
The four sections are:
1. Introduction
2. Advanced
3. Error management
4. Schemas

Each section has its own README file with a detailed explanation of the exercises. 
Please add the parent pom of the section as a Maven Project when you start to work on the exercises of that particular section.
The exercises are meant to be done in chronological order.