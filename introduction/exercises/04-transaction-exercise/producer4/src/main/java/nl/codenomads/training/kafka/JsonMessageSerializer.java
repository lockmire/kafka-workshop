package nl.codenomads.training.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serializer;

public class JsonMessageSerializer implements Serializer<Message> {

    private final ObjectWriter objectWriter;

    public JsonMessageSerializer() {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        objectWriter = mapper.writerFor(Message.class);
    }

    @Override
    public byte[] serialize(String topic, Message message) {
        try {
            return objectWriter.writeValueAsBytes(message);
        } catch (JsonProcessingException e) {
            throw new SerializationException("Could not serialize the message.", e);
        }
    }

}
