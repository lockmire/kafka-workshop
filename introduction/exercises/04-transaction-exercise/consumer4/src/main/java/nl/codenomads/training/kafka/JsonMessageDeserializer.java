package nl.codenomads.training.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.IOException;

public class JsonMessageDeserializer implements Deserializer<Message> {

    private final ObjectReader objectReader;

    public JsonMessageDeserializer() {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        objectReader = mapper.readerFor(Message.class);
    }

    @Override
    public Message deserialize(String topic, byte[] bytes) {
        try {
            return objectReader.readValue(bytes);
        } catch (IOException e) {
            throw new SerializationException("Couldn't read the JSON payload.", e);
        }
    }
}
