package nl.codenomads.training.kafka;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

public class Message {

  private final LocalDateTime timestamp;
  private final String text;

  public Message(String text) {
    this(LocalDateTime.now(), text);
  }

  @JsonCreator
  public Message(@JsonProperty("timestamp") LocalDateTime timestamp, @JsonProperty("text") String text) {
    this.timestamp = timestamp;
    this.text = text;
  }

  public LocalDateTime getTimestamp() {
    return timestamp;
  }

  public String getText() {
    return text;
  }

  @Override
  public String toString() {
    return "Message{" +
        "timestamp=" + timestamp +
        ", text='" + text + '\'' +
        '}';
  }
}
