Command Cheat Sheet Kafka 3.7.0
Below are listed some useful commands that can be executed from ${KAFKA_HOME}/bin:

Kafka-topics
./kafka-topics.sh --bootstrap-server localhost:9092 -list	List all topics.
./kafka-topics.sh --describe --bootstrap-server localhost:9092 --topic [topic]	Describe topic. Info such as: number of partitions, replication, leaders and more.
./kafka-topics.sh --delete --bootstrap-server localhost:9092 --topic [topic]	Delete topic.
./kafka-topics.sh --alter --bootstrap-server localhost:9092 --topic [topic] --partitions [number of partitions]	Change number of partitions of the topic.
./kafka-configs.sh --bootstrap-server localhost:9092 --entity-type topics --entity-name [topic] --alter --add-config retention.ms=[retention time in millis]  Change retention time of the topic.

Kafka-consumer-groups
./kafka-consumer-groups.sh --bootstrap-server localhost:9092 --list	List all consumer groups.
./kafka-consumer-groups.sh --bootstrap-server localhost:9092 --describe --group [consumer group]	Display information for the given consumer group, like the offset per partition.
./kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic [topic] --from-beginning	Display and tail messages coming on on the all partitions on the given topic since the beginning.
./kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic [topic] --partition [partition] --offset [offset]	Display and tail messages coming on on the given partition on the given topic since the given offset.
./kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic [topic] --property print.key=true --property key.separator="[key separator]" --from-beginning	Display and tail messages coming on the given topic also displaying the key of the message.
./kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic [topic] --property print.timestamp=true --from-beginning	Display and tail messages coming on the given topic also displaying the ingestion of the message. This is the timestamp of when the message is written on the log.

Kafka-console-producer
./kafka-console-producer.sh --broker-list localhost:9092 --topic [topic]	Produce record to the topic. Message key is empty
./kafka-console-producer.sh --broker-list localhost:9092 --topic [topic] --property parse.key=true --property key.separator=[key separator]	Produce record to the topic. Message key is empty

For documentation on Kafka CLI commands look at:
https://datacadamia.com/dit/kafka/kafka-console-consumer
https://datacadamia.com/dit/kafka/kafka-console-producer
